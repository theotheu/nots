# NotS

Belongs to assignment "Load Balancer"


## Quick start

- Install Docker
- Clone this repo
- Change directory to repo and run `docker-compose up`

That's it. 

# Step by step installation 
Not required for a running server farm


## Installation
- Run `npm install`

## Configuration
- Run `export PORT=8081; forever app.js`
- Repeat last step for every extra server you need

## Options
- Use `export INTERVALENABLED=1` to enable an alternating working server. Default value = 0 (never alternating, always working)
- Use `export TIMEOUT=60` to set the interval the server is running or not responding. Default value = 10 (seconds)

# Docker
- Run `docker build -t my-nodejs-app .`

## Option: start sessions with tmux
- Start a tmux session with `tmux -s nots1`
- Run `docker run -it -p 8081:3000 -e INTERVALENABLED=0 --rm --name my-running-app1 my-nodejs-app`

## Option: start sessions with docker-compose
- Run `docker-compose up -d`

## Clean up docker files

### Stop all containers
`docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)`

### Remove all containters
`docker rmi -f $(docker images -a -q)`

### BIG file on Mac OS X
```bash
docker rm $(docker ps -a -q)
docker rmi $(docker images -a -q)
docker volume rm $(docker volume ls |awk '{print $2}')
rm -rf ~/Library/Containers/com.docker.docker/Data/*
```

