/**
 * Created by theotheu on 30-05-16.
 */
/**
 * Part of last assigned NotS, load balancer
 * To test for sessions & other stuff
 */

var session = require('express-session'),
    express = require("express"),
    osHostname = require("os").hostname()
    ;

var app = express();
app.disable('x-powered-by');

var config = {
    port: 3000,
    intervalEnabled: 0,
    timeout: 10,
    cookieTimeout: 60
};

config.port = process.env.PORT || config.port;
config.intervalEnabled = process.env.INTERVALENABLED || config.intervalEnabled;
config.timeout = parseInt(process.env.TIMEOUT || config.timeout) * 1000 ;
config.cookieTimeout = parseInt(process.env.COOKIETIMEOUTINSECS || config.cookieTimeout) * 1000;


// Use the session middleware
app.use(session({
    proxy: true,
    resave: true,
    saveUninitialized: true,
    secret: 'keyboard cat', cookie: {maxAge: config.cookieTimeout}
}));
var enabled = true;
var timeout = config.timeout ;
var d = new Date();
// Access the session as req.session
app.get('/', function (req, res, next) {

    var delta = new Date() - d;

    console.log("\n\n>>>>>", osHostname);
    console.log("timeout: ", config.timeout);
    console.log("cookieTimeout: ", config.cookieTimeout);
    console.log("delta: ", delta, "delta > timeout: ", delta > timeout);
    console.log("<<<<<\n");


    if (config.intervalEnabled == 1 && !enabled) {
        res.destroy();
    }

    if (delta > timeout) {
        d = new Date();
        if (!enabled) {
            enabled = true;
        } else {
            enabled = false;
        }
    }

    var sess = req.session;
    res.setHeader('Content-Type', 'text/html');
    if (sess.views) {
        sess.views++;
        res.write('<p>Views: ' + sess.views + '</p>');
        res.write('<p>The session expires in: ' + (sess.cookie.maxAge)/1000 + 's</p>');
        if (config.intervalEnabled == 1) {
            res.write('<p>The http server disables in: ' + ((timeout - delta) / 1000) + 's</p>');
        } else {
            res.write('<p>Intervals are disabled. The server is always up and running.</p>');
        }



        res.end();
    } else {
        sess.views = 1;
        res.write('<p>welcome to the session demo. refresh!</p>');
    }
    res.write('<p>hostname: ' + osHostname + '</p>');
    res.end();
});

app.listen(config.port, function () {
    console.log("App Started on PORT " + config.port);
    console.log("Intervals enabled: " + config.intervalEnabled);
});

